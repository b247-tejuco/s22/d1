//[SECTION] Array Methods

	//Javascript has built-in functions and methods for arrays.
	//This allow us to manipuiulate and access array items.

	//[SECTION] Mutators Methods

		//Mutator methods are functions that 'mutate' or change
		//an array after they're created.

		let fruits= ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

			//[SUB-SECTION] push()
			//adds an element in the end of an array AND returns
			//the array's length
			/*
				Syntax:
					arrayName.push()
			
			*/

		console.log('Current array:');
		console.log(fruits);

		
		let fruitsLength= fruits.push('Mango');
		console.log(fruitsLength);
		console.log('Mutated array from push method:');
		console.log(fruits);

		//adding mltiple elems to an array

		let fruitsLength2= fruits.push('Avocado', 'Guava');
		console.log(fruitsLength2)
		console.log('Mutated array from push method:');
		console.log(fruits);

		//sub section pop()
		//Removes last elem and return removed elem
		/*syntax: arrayName.pop()*/

		let removedFruit= fruits.pop();
		console.log(removedFruit);
		console.log('Mutated array from pop method:')
		console.log(fruits);

		//SUB-SECTION Unshift()

		//adds one or more elements at the beginning of an array
		/*
			syntax: arrayName.unshift();
		*/

		fruits.unshift('Lime', 'Banana');
		console.log('Mutated array from unshift method:')
		console.log(fruits);

		//subsection shift()
		//removes an element at the beginning of the aarray
		/*
			syntax:
			arrayName.shift();
		*/

		let anotherRemovedFruit= fruits.shift();
		console.log(anotherRemovedFruit);
		console.log('Mutated array from shift method:');
		console.log(fruits);

		//subsection splice()
		//simultaneously removes elements from a specified index number
		//and adds elements

		fruits.splice(1,2, 'Lime', 'Cherry');
		console.log('Mutated array from splice method:');
		console.log(fruits);

		//subsection sort()
		//rearranges array elem in alphanumeric order

		fruits.sort();
		console.log('Sorted array from sort method: ');
		console.log(fruits);

		//subsection reverse()
		//reverses the order of array elements

		fruits.reverse();
		console.log('Sorted array from reverse method:');
		console.log(fruits);

	//SECTION Non Mutators Methods
	console.log('Non-Mutators Methods');
	//Non mutator methods are functions that do not modify or change
	//an array after theyre created

	let countries= ['US', 'PH', 'CAN', 'SG', 'TH', 'FR', 'DE', 'PH'];
	console.log(countries);


	// [SUB-SECTION] indexOf()
	//Returns the index number of the first matching element
	//found in an array
	//if no match was found, the result will be negative 1

		let firstIndex= countries.indexOf('PH');
		console.log('Result of indexOf Method: '+ firstIndex);

		let invalidCountry= countries.indexOf('CH');
		console.log('Result of indexOf Method: '+ invalidCountry);

	//SUBSECITON lastIndexOf()
	//returns the index number of the last matching element
	//found in an array

		let lastIndex= countries.lastIndexOf('PH');
		console.log('Result of lastIndexOf method: '+ lastIndex);

		let lastIndexStart= countries.lastIndexOf('PH', 7);
		console.log('Result of lastIndexOf method: '+lastIndexStart);

	//subsection slice()
	//portions/slices elements from an array AND returns a new array.

		let slicedArrayA= countries.slice(2);
		console.log('Result from slice method:');
		console.log(slicedArrayA); //Result will be ['CAN, SG, TH' etc]
		console.log(countries);


		let slicedArrayB= countries.slice(2, 6);
		console.log('Result from slice method:');
		console.log(slicedArrayB);

		//scopes elements from index 2 element to 6th element(not index)


		//slicingoff elems from last elem of an array

		let slicedArrayC= countries.slice(-3);
		console.log('Result from slice method:');
		console.log(slicedArrayC);

		//sub-section toString()

		//returns an array as a string separated by commas
		let stringArray= countries.toString();
		console.log('Result from toString Method: '+stringArray)

		console.log(countries);

		//subsection concat();
		//combines two arrays and returns the combined reuslt
			let tasksArrayA= ['drink html', 'eat javascript'];
			let tasksArrayB= ['inhale css', 'breathe sass'];
			let tasksArrayC= ['get git', 'be node'];
			
			let tasks= tasksArrayA.concat(tasksArrayB);
			console.log('Result from concat method:');
			console.log(tasks);


			//combining multiple arrays
			console.log('Result from concat method: ');
			let allTasks= tasksArrayA.concat(tasksArrayB, tasksArrayC);
			console.log(allTasks);

			//combining arrays with elements
			let combinedTasks= tasksArrayA.concat('smell express', 'throw react');
			console.log('Result from concat method:');
			console.log(combinedTasks);

		//subsection join()
		//returns an array as a string separated by specified separators

			let users=['John', 'Jane', 'Joe', 'Robert'];

			console.log(users.join());
			console.log(users.join(''));
			console.log(users.join(' - '));


	//sections iterations methods

		//iterations methods are loops designed to perform repetitive tasks
		//on arrays

		//sub section forEach()
			//similar to a for loop that iterates on each array element
			/*
				syntax: arrayName.forEach(function(indivElement){
							statement
				})
			*/

				allTasks.forEach(function(task1){
					console.log(task1);
				});

				//using forEach with conditional statements
					let filteredTask= [];

				//[SUBSECTION] Looping through all array items
					//creating a separat variable to store
					//results of array iteration methods
					//are also good practice to avoid confusion
					//by modifying the original array

					allTasks.forEach(function(task){

						//if statement
						if(task.length>10){
							filteredTask.push(task);
						}
					});

					console.log('Result of filtered tasks:');
					console.log(filteredTask);


				//subsection map()
					//iterates on each element AND returns new array
					//with diff values depending on the result of the
					//functon's operation

					let numbers= [1, 2, 3, 4, 5];
					let numbersMap= numbers.map(function(number){
						return number * 3;
					})

					console.log('Original Array:')
					console.log(numbers)
					console.log('Result of map method:')
					console.log(numbersMap)

					//subscection map() vs forEach()

					let numberForEach= numbers.forEach(function(number){
						return number * 3
					})

					console.log(numberForEach); //result will be uyndefined
					//forEach(), loops over all items in the array as does map(),
					//but forEach() does not return new array.

					//subsection every()
					//checks if all elements in an array meet the given condition
						let allValid= numbers.every(function(number){
							return(number < 10);
						})

						console.log('Result of every method:');
						console.log(allValid);

					//subsection some()
					//checks if atleast one element in the array meets the given condition

					let someValid= numbers.some(function(number){
						return (number < 2);
					});

					console.log('Result of some method:');
					console.log(someValid);

					//combining the returned result froim the every/some method
					//may be used in other statements to perform consectuve results

					// if(allValid){
					if(someValid){
						console.log('Some numbers in the array are greater than 2');
					}
					
					// subsectin filter()

					//returns new array that containes elements which meets the given condiiton
						let filterValid= numbers.filter(function(number){
							return(number < 3);
						})

							console.log('Result of filter method:');
							console.log(filterValid);

							let nothingFound= numbers.filter(function(number){
								return(number= 0);

							})
					
							console.log('Result of filter method:')
							console.log(nothingFound);

							//Filtering using foreach
							let filteredNumbers= [];

							numbers.forEach(function(number){
								if(number < 3){
									filteredNumbers.push(number);
								}
							})

							console.log('Result of filter method:')
							console.log(filteredNumbers);

						// [SUBSECTION] includes()

						//includes() method checks if the argument passed can be found in the array.

							let products=['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
							let productFound1= products.includes('Mouse');
							console.log(productFound1);

							let productFound2= products.includes('Headset');
							console.log(productFound2);


						/*
						-Methods can be 'chained' using them one after another
						-The result of the first method is used on the second method until all
						'chained' methods have been resolved.
						*/

						let filteredProducts= products.filter(function(product){
							return product.toLowerCase().includes('a', 'o');

						})
						console.log(filteredProducts);

						//subsection reduce()
							//evaluates elements from left to right and returns
							//reduces the array into a single value
							//how the reduce method works
							/*
							1. the first elem in the array is stored in the 'accumulator' parameter
							2. the second/next elem in the array is stored in the 'currentValue' parameter
							3. an operation is performed on the two elements
							4. the loop repeats step 1-3 until all elements have been worked on

							*/

							let iteration= 0;
								console.log(numbers);
								// let numbers1= [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

							let reducedArray= numbers.reduce(function(x, y){
								console.warn('current iteration: '+ ++iteration);
								console.log('accumulator: '+ x);
								console.log('currentValue: '+ y); //just explainedwhat hapend

								return x + y;
							})

							console.log('Result of reduce method: '+ reducedArray);

							//Reducing String Arrays
								let list = ['Hello', 'Again', 'World'];

								let reducedJoin= list.reduce(function(x,y){
									return x+ ' '+y;
								});

								console.log('Result of reduced method: '+ reducedJoin);
			

								
									